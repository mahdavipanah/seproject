var mysql = require('mysql');


var conn = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'seproject',
    password: '123456',
    database: 'seproject',
    supportBigNumbers: true,
    multipleStatements: true,
    charset: 'utf8mb4_persian_ci'
});


module.exports.conn = conn;

