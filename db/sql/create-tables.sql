CREATE TABLE IF NOT EXISTS `users` (
  `id`       MEDIUMINT UNSIGNED         NOT NULL PRIMARY KEY  AUTO_INCREMENT,
  `name`     VARCHAR(40)
             CHARACTER SET utf8mb4
             COLLATE utf8mb4_persian_ci NOT NULL,
  `phone`    VARCHAR(11) NOT NULL,
  `address`  TEXT CHARACTER SET utf8mb4
             COLLATE utf8mb4_persian_ci NOT NULL
)
  ENGINE = INNODB;
-- ---------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `products` (
  `id`           MEDIUMINT UNSIGNED         NOT NULL PRIMARY KEY  AUTO_INCREMENT,
  `title`        VARCHAR(40)
                 CHARACTER SET utf8mb4
                 COLLATE utf8mb4_persian_ci NOT NULL,
  `copies_count` INT                        NOT NULL,
  `price`        INT                        NOT NULL,
  `available`    BOOLEAN                    NOT NULL              DEFAULT TRUE
)
  ENGINE = INNODB;
-- ---------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `admins` (
  `id`       MEDIUMINT UNSIGNED         NOT NULL PRIMARY KEY  AUTO_INCREMENT,
  `name`     VARCHAR(40)
             CHARACTER SET utf8mb4
             COLLATE utf8mb4_persian_ci NOT NULL,
  `username` VARCHAR(40)
             CHARACTER SET utf8mb4
             COLLATE utf8mb4_persian_ci NOT NULL,
  `phone`    VARCHAR(11)                NOT NULL,
  `address`  TEXT CHARACTER SET utf8mb4
             COLLATE utf8mb4_persian_ci NOT NULL,
  `manager`  BOOLEAN                    NOT NULL              DEFAULT FALSE,
  `password` VARCHAR(60)                NOT NULL
)
  ENGINE = INNODB;
-- ---------------------------------------------------------------------------
