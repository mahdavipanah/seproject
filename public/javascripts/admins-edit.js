$(document).ready(function (e) {
    $('.ui.checkbox').checkbox();

    // custom form validation rule
    $.fn.form.settings.rules.passwordEditRule = function(value) {
        return value === '' || String(value).length >= 6
    };

    $('.ui.form')
        .form({
            fields: {
                'name': {
                    identifier: 'name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'نام نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'username': {
                    identifier: 'username',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'یوزرنیم نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'phone': {
                    identifier: 'phone',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'تلفن نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'integer',
                            prompt: 'تلفن باید عدد باشد'
                        },
                        {
                            type: 'exactLength[11]',
                            prompt: 'تلفن باید ۱۱ رقم باشد'
                        }
                    ]
                },
                'address': {
                    identifier: 'address',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'آدرس نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'password': {
                    identifier: 'password',
                    rules: [
                        {
                            type: 'passwordEditRule',
                            prompt: 'رمز عبور یا باید خالی باشد یا طول حداقل ۶ داشته باشد'
                        }
                    ]
                }
            }
        });
});
