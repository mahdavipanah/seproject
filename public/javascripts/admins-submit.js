$(document).ready(function (e) {
    $('.ui.checkbox').checkbox();

    $('.ui.form')
        .form({
            fields: {
                'name': {
                    identifier: 'name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'نام نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'username': {
                    identifier: 'username',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'یوزرنیم نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'phone': {
                    identifier: 'phone',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'تلفن نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'integer',
                            prompt: 'تلفن باید عدد باشد'
                        },
                        {
                            type: 'exactLength[11]',
                            prompt: 'تلفن باید ۱۱ رقم باشد'
                        }
                    ]
                },
                'address': {
                    identifier: 'address',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'آدرس نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'password': {
                    identifier: 'password',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'رمز عبور نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'minLength[6]',
                            prompt: 'رمز عبور باید حداقل طول ۶ داشته باشد'
                        }
                    ]
                }
            }
        });
});
