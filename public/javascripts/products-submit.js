$(document).ready(function (e) {
    $('.ui.checkbox').checkbox();

    $('.ui.form')
        .form({
            fields: {
                'title': {
                    identifier: 'title',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'عنوان محصول نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'copiesCount': {
                    identifier: 'copiesCount',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'تعداد محصول نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'integer',
                            prompt: 'تعداد محصول باید عدد باشد'
                        }
                    ]
                },
                'price': {
                    identifier: 'price',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'قیمت محصول نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'integer',
                            prompt: 'قیمت محصول باید عدد باشد'
                        }
                    ]
                }
            }
        });
});
