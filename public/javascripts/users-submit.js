$(document).ready(function (e) {
    $('.ui.form')
        .form({
            fields: {
                'name': {
                    identifier: 'name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'نام و نام خانوادگی نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'phone': {
                    identifier: 'phone',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'تلفن نمی‌تواند خالی باشد'
                        },
                        {
                            type: 'exactLength[11]',
                            prompt: 'تلفن باید طول ۱۱ داشته باشد'
                        },
                        {
                            type: 'integer',
                            prompt: 'تلفن باید عدد باشد'
                        }
                    ]
                },
                'address': {
                    identifier: 'address',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'آدرس نمی‌تواند خالی باشد'
                        }
                    ]
                }
            }
        });
});
