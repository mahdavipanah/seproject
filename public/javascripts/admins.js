$(document).ready(function (e) {

    $('.ui.checkbox').checkbox();

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });

    setTimeout(function(){
        if (!$('.message').hasClass('hidden')) {
            $('.message .close').click();
        }
    }, 5000);

    $('.delete-admins.button').click(function () {
        if (confirm("آیا از حذف این ادمین مطمئن هستید؟")) {
            var id = $($(this).parent().parent().find('.admin-id')[0]).html();
            $('#admin-delete-id').val(id);
            $('#admins-delete-form').submit();
        }
    });

});
