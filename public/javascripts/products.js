$(document).ready(function (e) {

    $('.ui.checkbox').checkbox();

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });

    setTimeout(function(){
        if (!$('.message').hasClass('hidden')) {
            $('.message .close').click();
        }
    }, 5000);

    $('.delete-products.button').click(function () {
        if (confirm("آیا از حذف این محصول مطمئن هستید؟")) {
            var id = $($(this).parent().parent().find('.product-id')[0]).html();
            $('#product-delete-id').val(id);
            $('#products-delete-form').submit();
        }
    });

});
