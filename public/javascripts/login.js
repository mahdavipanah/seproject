$(document).ready(function (e) {

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });

    setTimeout(function(){
        if (!$('.message').hasClass('hidden')) {
            $('.message .close').click();
        }
    }, 5000);

    $('.ui.form')
        .form({
            fields: {
                'username': {
                    identifier: 'username',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'نام کاربری نمی‌تواند خالی باشد'
                        }
                    ]
                },
                'password': {
                    identifier: 'password',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'رمز عبور نمی‌تواند خالی باشد'
                        }
                    ]
                }
            }
        });

});
