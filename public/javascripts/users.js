$(document).ready(function (e) {

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });

    setTimeout(function(){
        if (!$('.message').hasClass('hidden')) {
            $('.message .close').click();
        }
    }, 5000);

    $('.delete-users.button').click(function () {
        if (confirm("آیا از حذف این کاربر مطمئن هستید؟")) {
            var id = $($(this).parent().parent().find('.user-id')[0]).html();
            $('#user-delete-id').val(id);
            $('#users-delete-form').submit();
        }
    });

});
