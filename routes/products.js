var express = require('express');
var productsModel = require('../models/products');

var router = express.Router();


router.get('/products', function (req, res) {
    productsModel.getProducts(function (products) {
        res.render('products', {products: products});
    });
});


router.get('/products/submit', function (req, res) {
    res.render('products-submit');
});


router.post('/products/submit', function (req, res) {
    productsModel.createProduct(
        req.body.title,
        req.body.copiesCount,
        req.body.price,
        !!req.body.available,
        function (id) {
            res.redirect('/products?new_id=' + id);
        }
    );
});


router.get('/products/edit/:id', function (req, res, next) {
    productsModel.getProduct(req.params.id, function (product) {
        // No product with given id found
        if (!product)
            return next(404);

        res.render('products-edit', {product: product});
    });
});


router.post('/products/edit/:id', function (req, res) {
    productsModel.editProduct(
        req.params.id,
        req.body.title,
        req.body.copiesCount,
        req.body.price,
        !!req.body.available,
        function () {
            res.redirect('/products?editId=' + req.params.id);
        }
    );
});


router.post('/products/delete', function (req, res) {
    productsModel.deleteProduct(req.body.id, function () {
        res.redirect('/products?deleteMessage');
    });
});


module.exports = router;
