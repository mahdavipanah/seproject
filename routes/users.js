var express = require('express');
var usersModel = require('../models/users');

var router = express.Router();


router.get('/users', function (req, res) {
    usersModel.getUsers(function (users) {
        res.render('users', {users: users});
    });
});


router.get('/users/submit', function (req, res) {
    res.render('users-submit');
});


router.post('/users/submit', function (req, res) {
    usersModel.createUser(
        req.body.name,
        req.body.phone,
        req.body.address,
        function (id) {
            res.redirect('/users?new_id=' + id);
        }
    );
});


router.get('/users/edit/:id', function (req, res, next) {
    usersModel.getUser(req.params.id, function (user) {
        // No user with given id found
        if (!user)
            return next(404);

        res.render('users-edit', {user: user});
    });
});


router.post('/users/edit/:id', function (req, res) {
    usersModel.editUser(
        req.params.id,
        req.body.name,
        req.body.phone,
        req.body.address,
        function () {
            res.redirect('/users?editId=' + req.params.id);
        }
    );
});


router.post('/users/delete', function (req, res) {
    usersModel.deleteUser(req.body.id, function () {
        res.redirect('/users?deleteMessage');
    });
});


module.exports = router;
