var bcrypt = require('bcryptjs');
var adminsModel = require('../models/admins');

var router = require('express').Router();


router.get('/login', function (req, res) {
    res.render('login');
});


router.post('/login', function (req, res) {
    adminsModel.getAdminByUsername(req.body.username, function (admin) {
        // If an admin with given username found
        if (admin)
            bcrypt.compare(req.body.password, admin.password, function (err, matches) {
                // If password matches
                if (matches) {
                    req.session.userType = admin.manager ? 'manager' : 'clerk';
                    res.redirect('/users');
                } else
                    res.render('login', {loginError: true});
            });
        else
            res.render('login', {loginError: true});
    });
});


router.get('/logout', function (req, res) {
    req.session.destroy(function () {
        res.redirect('/login');
    });
});


module.exports = router;
