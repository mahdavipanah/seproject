var express = require('express');
var adminsModel = require('../models/admins');

var router = express.Router();


router.get('/admins', function (req, res) {
    adminsModel.getAdmins(function (admins) {
        res.render('admins', {admins: admins});
    });
});


router.get('/admins/submit', function (req, res) {
    res.render('admins-submit');
});


router.post('/admins/submit', function (req, res) {
    adminsModel.createAdmin(
        req.body.name,
        req.body.username,
        req.body.phone,
        req.body.address,
        !!req.body.manager,
        req.body.password,
        function (id) {
            res.redirect('/admins?new_id=' + id);
        }
    );
});


router.get('/admins/edit/:id', function (req, res, next) {
    adminsModel.getAdmin(req.params.id, function (admin) {
        // No admin with given id found
        if (!admin)
            return next(404);

        res.render('admins-edit', {admin: admin});
    });
});


router.post('/admins/edit/:id', function (req, res) {
    adminsModel.editAdmin(
        req.params.id,
        req.body.name,
        req.body.username,
        req.body.phone,
        req.body.address,
        !!req.body.manager,
        req.body.password === "" ? null : req.body.password,
        function () {
            res.redirect('/admins?editId=' + req.params.id);
        }
    );
});


router.post('/admins/delete', function (req, res) {
    adminsModel.deleteAdmin(req.body.id, function () {
        res.redirect('/admins?deleteMessage');
    });
});


module.exports = router;
