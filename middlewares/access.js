var router = require('express').Router();


/*
 In order to access a page (except for login page
 and public files) User must be logged in. If not,
 redirect to login page.
 */
router.use(
    ['/users', '/products', '/admins', '/404'],
    function (req, res, next) {
        // If user is not logged in
        if (!req.session.userType)
            return res.redirect('/login');

        next();
    }
);

// Only admins can access products page
router.use(
    ['/products', '/admins'],
    function (req, res, next) {
        if (req.session.userType !== 'manager')
            return res.redirect('/users');

        next();
    }
);


module.exports = router;
