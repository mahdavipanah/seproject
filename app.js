var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var session = require('express-session');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'ssshhhhh',
    resave: false,
    saveUninitialized: false
}));

app.use(function (req, res, next) {
  res.locals.req = req;
  res.locals.res = res;

  next();
});

app.use(require('./middlewares/access'));


app.use(require('./routes/users'));
app.use(require('./routes/products'));
app.use(require('./routes/admins'));
app.use(require('./routes/login'));


app.get('/404', function (req, res) {
    res.render('404');
});


// Catch 404
app.use(function (req, res) {
  res.redirect('/404');
});
app.use(function (err, req, res, next) {
  if (err === 404)
    return res.redirect('/404');

  next();
});

module.exports = app;
