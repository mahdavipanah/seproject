var db = require('../db');


module.exports.createProduct = function (title, copiesCount, price, available, callback) {
    db.conn.query(
        " INSERT INTO `products` (`title`, `copies_count`, `price`, `available`)" +
        " VALUES (?, ?, ?, ?)",
        [title, copiesCount, price, available],
        function (err, results) {
            callback(results.insertId);
        }
    );
};

module.exports.getProducts = function (callback) {
    db.conn.query(
        "SELECT * FROM `products`",
        function (err, results) {
            callback(results);
        }
    );
};


module.exports.getProduct = function (id, callback) {
    db.conn.query(
        "SELECT * FROM `products` WHERE `id` = ?",
        id,
        function (err, results) {
            callback(results[0]);
        }
    );
};


module.exports.editProduct = function (id, title, copiesCount, price, available, callback) {
    db.conn.query(
        "UPDATE `products` SET `title` = ?, `copies_count` = ?, `price` = ?, `available` = ? WHERE `id` = ?",
        [title, copiesCount, price, available, id],
        callback
    );
};


module.exports.deleteProduct = function (id, callback) {
    db.conn.query(
        "DELETE FROM `products` WHERE `id` = ?",
        id,
        callback
    );
};
