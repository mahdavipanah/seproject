var db = require('../db');


module.exports.createUser = function (name, phone, address, callback) {
    db.conn.query(
        "INSERT INTO `users` (`name`, `phone`, `address`) VALUES (?, ?, ?)",
        [name, phone, address],
        function (err, results) {
            callback(results.insertId);
        }
    );
};


module.exports.getUsers = function (callback) {
    db.conn.query(
        "SELECT * FROM `users`",
        function (err, results) {
            callback(results);
        }
    );
};


module.exports.getUser = function (id, callback) {
    db.conn.query(
        "SELECT * FROM `users` WHERE `id` = ?",
        id,
        function (err, results) {
            callback(results[0]);
        }
    );
};


module.exports.editUser = function (id, name, phone, address, callback) {
    db.conn.query(
        "UPDATE `users` SET `name` = ?, `phone` = ?, `address` = ? WHERE `id` = ?",
        [name, phone, address, id],
        callback
    );
};


module.exports.deleteUser = function (id, callback) {
    db.conn.query(
        "DELETE FROM `users` WHERE `id` = ?",
        id,
        callback
    );
};
