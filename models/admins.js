var db = require('../db');
var bcrypt = require('bcryptjs');


module.exports.createAdmin = function (name, username, phone, address, manager, password, callback) {
    bcrypt.hash(password, 8, function (err, hashedPassword) {
        db.conn.query(
            " INSERT INTO `admins` (`name`, `username`, `phone`, `address`, `manager`, `password`)" +
            " VALUES (?, ?, ?, ?, ?, ?)",
            [name, username, phone, address, manager, hashedPassword],
            function (err, results) {
                callback(results.insertId);
            }
        );
    });
};

module.exports.getAdmins = function (callback) {
    db.conn.query(
        "SELECT * FROM `admins`",
        function (err, results) {
            callback(results);
        }
    );
};


module.exports.getAdmin = function (id, callback) {
    db.conn.query(
        "SELECT * FROM `admins` WHERE `id` = ?",
        id,
        function (err, results) {
            callback(results[0]);
        }
    );
};


module.exports.getAdminByUsername = function (username, callback) {
    db.conn.query(
        "SELECT * FROM `admins` WHERE `username` = ?",
        username,
        function (err, results) {
            callback(results[0]);
        }
    );
};


module.exports.editAdmin = function (id, name, username, phone, address, manager, password, callback) {
    function submitEdit() {
        db.conn.query(
            "UPDATE `admins` SET `name` = ?, `username` = ?, `phone` = ?, `address` = ?, `manager` = ?"  +
            (password !== null ? ", `password` = ?" : '')
            + " WHERE `id` = ?",
            [name, username, phone, address, manager].concat(password !== null ? password : []).concat(id),
            callback
        );
    }

    // If password should get updated
    if (password)
        bcrypt.hash(password, 8, function (err, hashedPassword) {
            password = hashedPassword;
            submitEdit();
        });
    // If password doesn't need to get updated
    else
        submitEdit();
};


module.exports.deleteAdmin = function (id, callback) {
    db.conn.query(
        "DELETE FROM `admins` WHERE `id` = ?",
        id,
        callback
    );
};
